// console.log("Hello World");

// Functions 

	// Parameters and Arguments

		function printInput() {
			let nickName = prompt("Enter you nickname: ")
			console.log("Hi, " + nickName);
		}
		// printInput();

		// However, for other cases, functions can also process data directly passed into it instead of relying on global variables and prompt();

		function printName(name) {
			console.log("My name is " + name);
		}
		printName("Juana");

		// you can directly pass data into the dunction
		// the function can then call/use that data which is referred as "name" within the function
		// "name" is called a parameter
		// a "parameter" acts as a named variable/container that exist only inside of a function
		// it is used to store information that is provided to a function when it is called or invoked

		// "Juana", the information/data provided directly into a function is called an argument

		// in the following examples, "John" and "Jane" are both aruguments since both of them are supplied as information that will be used to print out our message

		printName("John");
		printName("Jane");

		let sampleVariable = "Yui"
		printName(sampleVariable);

		// Function arguments cannot be used by a function if there is no parameters provided within the function.

		function checkDivisibilityBy8(num) {
			let remainder = num  % 8;
			console.log("The remainder of " + num + " is divided by 8 is " +remainder);
			let isDivisibleBy8 = remainder === 0;
			console.log("Is " + num + " divisible by 8?");
			console.log(isDivisibleBy8);
		}
		checkDivisibilityBy8(64);
		checkDivisibilityBy8(28);

		/*
		mini-activity
			create a function that can check the divisibility by 5
			1.56
			2.95
		*/

	function checkDivisibilityBy4(num2) {
			let remainder = num2  % 4;
			// console.log("The remainder of " + num2 + " is divided by 4 is " +remainder);
			let isDivisibleBy4 = remainder === 0;
			console.log("Is " + num2 + " divisible by 4?");
			console.log(isDivisibleBy4);
		}
		checkDivisibilityBy4(56);
		checkDivisibilityBy4(95);

		// you can also do the same using prompt(), however, take note that prompt(outputs) a string. Strings are not ideal for mathematical computations

		// functions as arguments
		// Function parameters can also accept other functions as arguments
		// Some complex functions used other functions as arhuments to perform more complicated results
		// This will be further seen when we discuss array methods

		function argumentFunction() {
			console.log("This function was passed as an argument befoer the messahe was printed");
		}

		function invokeFunction(argumentFunction) {
			argumentFunction();
		}

		// Adding and removing the parenthesis "()" imoacts the output of JS heabily
		// when a function is used with parenthesis "()", it denotes invoking/calling a function

		invokeFunction(argumentFunction);
		// a function used without a parenthesise is normally associated with using the function as an argument to another function

		console.log(argumentFunction); 
		// or finding more information abot a function in the console using console.log()


		// Using Multiple Parameters
		// multiple "arguments" will correspond to the number of the "parameters" decalred in a dunction in succeding order


		function createFullName(firstName, middleName, lastName) {
			console.log(firstName + " " + middleName + " " + lastName);
		} 
		createFullName("Juan", "Dela", "Cruz");

		// in JS, providing more/less arguments than the expected parameters will NOT return an error

		createFullName("Juan", "Dela");
		createFullName("Jane", "Dela", "Cruz", "Hello");

		// we can use caribles as arguments
		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName, middleName, lastName);

		function printFullName(middleName, firstName, lastName){
			console.log(firstName + " " + middleName + " " + lastName);
		}
		printName("Juan", "Dela" , "Cruz");
		// results to "Dela Juan Cruz" because "Juan" was received as middleName, "Dela" as first name

		/*
			create a function called printFriends
			3 parameters
			My three friends are: friend1, friend2, friend2.

			5mins
		*/

		function printFriends(friend1, friend2, friend3) {
			console.log("My three friends are: " + friend1 + ", " + friend2 + ", " + friend3);
		} 
		printFriends("Rio", "Pey", "Anne");

		// The return Statement
			// the return statement allows us to output a value from a fuction to be passed to the line/block of code that invoked/called the function


			function returnFullName(firstName, middleName, lastName) {
				// console.log(firstName + " " + middleName + " " + lastName);
				return firstName + " " + middleName + " " + lastName;
				console.log("This messgae will not be printed");
			}
			// notice that the console.log after the return is no longer printed on the console...
			// that is because ideallly line/block of code that comes after the retirn statment is ignored becayse it ends the function execution



			// in our example, the "return" sattement allows us to output a value of a function to be passed line/block of code that called tehefunction
			// the "returnFullName" function was called in the same line as decaling a variable

			// whatever value is returned from "returnFullName" function is stored in the "completeName" variable

			let completeName = returnFullName ("Jeffrey", "Smith", "Bezos");
			console.log(completeName);

			let completeName2 = returnFullName("Nejemiah", "C", "Ellorico");
			console.log(completeName2);

			let combination = completeName + completeName2;
			console.log(combination);

			// this way, a function is able to return a value we can further use or manipulate in our program instead of only printing/displaying it in the console

			console.log(returnFullName(firstName,middleName,lastName));
			// in this example, console.log() will print the returned of the returnFullName() function

			function returnAddress(city, country) {
				let fullAddress = city + ", " + country;
				return fullAddress;
			}
			let myAddress = returnAddress("Cebu City", "Philippines");
			console.log(myAddress);

			function printPlayerInfo(username,level,job) {
				console.log("Username: " + username);
				console.log("Level: " + level);
				console.log("Job: " + job);
				// return ("Username: " + username + "Level: " + level + "Job: " + job);
			}

			let user1 = printPlayerInfo("knight_white", 95, "Paladin");
			console.log(user1) // undefined

			/*
				create a function that will be able to able to multiple 1 numbers

			*/

			function printMultiplyTwoNumbers(a,b) {
				return a*b;
			}
		
			let product = printMultiplyTwoNumbers(5,4);
			console.log("The product is: ");
			console.log(product);
